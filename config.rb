http_path = "/"
css_dir = "skin/frontend/levelup/default/css"
sass_dir = "skin/frontend/levelup/default//scss"
images_dir = "skin/frontend/levelup/default/images"
javascripts_dir = "skin/frontend/levelup/default/js"
fonts_dir = "skin/frontend/levelup/default/fonts"

output_style = :expanded # :expanded or :nested or :compact or :compressed
environment = :development

line_comments = false
cache = true
color_output = false # required for mixture

Sass::Script::Number.precision = 7 # chrome needs a precision of 7 to round properly
# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass
